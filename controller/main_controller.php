<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 24/02/2018
 * Time: 03:41
 */

namespace jles\stockcount\controller;

class main_controller
{
	/** @var \phpbb\template\template */
	protected $template;

	/** @var \phpbb\config\db_text */
	protected $config_text;

	public function __construct(\phpbb\template\template $template, \phpbb\config\db_text $config_text)
	{
		$this->template = $template;
		$this->config_text = $config_text;
	}

	public function handle_bandeau_actionnaires()
	{
// get data from configtext
		$bandeau_text_data = $this->config_text->get_array(array(
			'pharm_nb_actionnaires',
			'pharm_nb_actions',
			'pharm_percent_share',
			'pharm_display_banner'
		));

		$this->template->assign_vars(array(
			'S_NB_ACTIONNAIRES'             => $bandeau_text_data['pharm_nb_actionnaires'],
			'S_NB_ACTIONS'                  => number_format($bandeau_text_data['pharm_nb_actions'], 0, ',', ' '),
			'S_PERCENT_SHARE'               => $bandeau_text_data['pharm_percent_share'],
			'S_ACP_PHARM_DISPLAY_BANNER'    => $this->config['acp_pharm_display_banner'],
			'S_USER_OWNS'                   => $this->user->data['user_ownpharm']
		));

		page_header('');
		$this->template->set_filenames(array(
				'body' => 'stockcount/bandeau_actionnaires.html')
		);
		page_footer();
	}
}