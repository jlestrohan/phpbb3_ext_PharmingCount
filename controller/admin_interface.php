<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 25/02/2018
 * Time: 14:23
 */

namespace jles\stockcount\controller;

interface admin_interface
{
	/**
	 * Display the options a user can configure for this extension
	 *
	 * @return void
	 * @access public
	 */
	public function display_options();

	/**
	 * Display the stock holders top 10
	 *
	 * @return mixed
	 */
	public function display_holders();
}