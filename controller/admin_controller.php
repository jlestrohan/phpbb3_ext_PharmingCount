<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 25/02/2018
 * Time: 14:21
 */

namespace jles\stockcount\controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use phpbb\includes\functions_content;

class admin_controller implements admin_interface
{
	/** @var string Custom form action */
	protected $u_action;


	/** @var \phpbb\request\request */
	protected $request;

	/** @var \phpbb\language\language */
	protected $lang;

	/** @var \phpbb\template\template */
	protected $template;

	/** @var \phpbb\config\config */
	protected $config;

	/** @var \phpbb\user */
	protected $user;

	/** @var \phpbb\log\log */
	protected $log;

	/** @var \phpbb\cache\service */
	protected $cache;

	/** @var \phpbb\pagination */
	protected $pagination;

	/** @var \phpbb\db\driver\driver_interface */
	protected $db;

	/** @var \phpbb\config\db_text */
	protected $config_text;

	/**
	 * Constructor
	 *
	 * @param \phpbb\request\request            $request
	 * @param \phpbb\language\language          $lang
	 * @param \phpbb\template\template          $template
	 * @param \phpbb\config\config              $config
	 * @param \phpbb\user                       $user
	 * @param \phpbb\log\log                    $log
	 * @param \phpbb\cache\service              $cache
	 * @param \phpbb\pagination                 $pagination
	 * @param \phpbb\db\driver\driver_interface $db
	 * @param \phpbb\config\db_text             $config_text
	 */
	public function __construct(
		\phpbb\request\request $request, \phpbb\language\language $lang, \phpbb\template\template $template,
		\phpbb\config\config $config, \phpbb\user $user, \phpbb\log\log $log, \phpbb\cache\service $cache, \phpbb\pagination $pagination,
		\phpbb\db\driver\driver_interface $db, \phpbb\config\db_text $config_text
	) {
		$this->request    = $request;
		$this->lang       = $lang;
		$this->template   = $template;
		$this->config     = $config;
		$this->user       = $user;
		$this->log        = $log;
		$this->cache      = $cache;
		$this->pagination = $pagination;
		$this->db         = $db;
		$this->config_text = $config_text;
	}

	/**
	 * Display the options a user can configure for this extension
	 */
	public function display_options()
	{
		// Create a form key for preventing CSRF attacks
		add_form_key( 'jles/pharming' );

		// Create an array to collect errors that will be output to the user
		$errors = array();

		if ( $this->request->is_set_post( 'submit' ) )
		{
			// Test if the submitted form is valid
			if ( ! check_form_key( 'jles/pharming' ) )
			{
				$errors[] = $this->lang->lang( 'FORM_INVALID' );
			}

			// If no errors, process the form data
			if ( empty( $errors ) )
			{
				// Set the options the user configured
				$this->set_options();

				// Add option settings change action to the admin log
				$this->log->add( 'admin', $this->user->data['user_id'], $this->user->ip, 'ACP_STOCKCOUNT_SETTINGS_LOG' );

				// Option settings have been updated and logged
				// Confirm this to the user and provide link back to previous page
				trigger_error( $this->lang->lang( 'ACP_PHARMING_SETTING_SAVED' ) . adm_back_link( $this->u_action ) );
			}
		}

		$s_errors = (bool) count( $errors );

		// Set output vars for display in the template
		$this->template->assign_vars( array(

			'S_STOCKCOUNT_ERROR'                => $s_errors,
			'STOCKCOUNT_ERROR_MSG'              => $s_errors ? implode( '<br />', $errors ) : '',
			'U_ACTION'                          => $this->u_action,
			'S_ACP_PHARM_ACTIVATE_EXTENSION'    => $this->config['acp_pharm_activate_extension'],
			'S_ACP_PHARM_TOTAL_STOCK_AVAILABLE' => (string) $this->config['acp_pharm_total_stock_available'],
			'S_ACP_PHARM_DISPLAY_BANNER'        => $this->config['acp_pharm_display_banner'],
			//'S_ACP_PHARM_CALC_PERCENTAGE'           => number_format(100 * $this->config['acp_pharm_warning_threshold'] / $this->config['acp_pharm_total_stock_available'],4) ,
			'S_ACP_PHARM_BANNER_REFRESH_DELAY'  => $this->config['acp_pharm_banner_refresh_delay'] / 1000,
		) );

	}


	/**
	 * Set the options a user can configure
	 */
	protected function set_options()
	{
		// Validate Pharming stock number
		$pharming_stock_number = $this->request->variable( 'acp_pharm_total_stock_available', '' );
		if ( empty( $pharming_stock_number ) )
		{
			trigger_error( $this->lang->lang( 'ACP_STOCKCOUNT_EMPTY_TOTAL_STOCK' ) . adm_back_link( $this->u_action ), E_USER_WARNING );
		}

		$this->config->set( 'acp_pharm_activate_extension', $this->request->variable( 'acp_pharm_activate_extension', 0 ) );
		$this->config->set( 'acp_pharm_total_stock_available', abs( intval( $this->request->variable( 'acp_pharm_total_stock_available', '' ) ) ) );
		$this->config->set( 'acp_pharm_display_banner', $this->request->variable( 'acp_pharm_display_banner', 0 ) );
		$this->config->set( 'acp_pharm_banner_refresh_delay', $this->request->variable( 'acp_pharm_banner_refresh_delay', 0 ) * 1000 );
	}

	/**
	 * Display the top_10 users who own stock options
	 */
	public function display_holders()
	{
		// Sorting

		$total_users   = $this->get_total_users();
		$total_holders = $this->get_total_holders();
		$total_in_avocat = 0; // d�compte des membres dans le groupe avocat

		if ( $this->request->is_set_post( 'submit' ) )
		{

		}

		$start          = $this->request->variable( 'start', 0 );
		$pagination_url = $this->u_action;

		// 6 = groupe robots, 1 = anonymes
		$sql = 'SELECT * FROM ' . USERS_TABLE . ' WHERE group_id <> 6 AND group_id <> 1  ORDER BY user_pharm_countowned DESC';

		$result = $this->db->sql_query_limit( $sql, $this->config['topics_per_page'], $start );

		// it�ration et affichage des donn�es
		while ( $row = $this->db->sql_fetchrow( $result ) )
		{
			// check si le user fait partie du groupe avocats
			$is_in_avocats_group = false;
			$sql_group = 'SELECT group_id
			FROM ' . USER_GROUP_TABLE . '
			WHERE user_id = ' . $row['user_id'] . '
				AND user_pending = 0';

			$result_group = $this->db->sql_query($sql_group);
			while ($group_id = $this->db->sql_fetchfield('group_id'))
			{
				// 8 = groupe avocats
				if($group_id == 8) {
					$is_in_avocats_group = true;
				}
			}
			$this->db->sql_freeresult($result_group);

			// calcul de STOCK_TOTAL_DIFF
			$total_diff = ( $row['user_pharm_countowned'] + $row['user_pharm_countfriend1'] + $row['user_pharm_countfriend2'] + $row['user_pharm_countfriend3'] +
			                $row['user_pharm_countfriend4'] + $row['user_pharm_countfriend5'] ) -
			              ( $row['pharm_lastowned_stocks'] + $row['pharm_lastfriend1_stocks'] + $row['pharm_lastfriend2_stocks'] +
			                $row['pharm_lastfriend3_stocks'] + $row['pharm_lastfriend4_stocks'] + $row['pharm_lastfriend5_stocks'] );

			$this->template->assign_block_vars( 'holders', array(
					'USER_NAME'         => $row['username'],
					'STOCK_COUNT'       => number_format($row['user_pharm_countowned'], 0, ',', ' '),
					'STOCK_F1_COUNT'    => number_format($row['user_pharm_countfriend1'], 0, ',', ' '),
					'STOCK_F2_COUNT'    => number_format($row['user_pharm_countfriend2'], 0, ',', ' '),
					'STOCK_F3_COUNT'    => number_format($row['user_pharm_countfriend3'], 0, ',', ' '),
					'STOCK_F4_COUNT'    => number_format($row['user_pharm_countfriend4'], 0, ',', ' '),
					'STOCK_F5_COUNT'    => number_format($row['user_pharm_countfriend5'], 0, ',', ' '),
					'STOCK_TOTAL_DIFF'  => number_format($row['user_pharm_last_edited'], 0, ',', ' ') > 0 ? $total_diff : '-',
					'STOCK_LAST_EDITED' => $row['user_pharm_last_edited'] > 0 ? gmdate( "d/m/Y - H:m", $row['user_pharm_last_edited'] ) : '-',
					'STOCK_AVOCATS'     => $is_in_avocats_group ? '<img src="/ext/jles/stockcount/adm/style/images/coche.png" />' : '',
					'STOCK_USER_OWN_PHARM'  => $row['user_ownpharm'],
				)
			);
		}
		$this->db->sql_freeresult( $result );

		$start = $this->pagination->validate_start( $start, $this->config['topics_per_page'], $total_holders );
		$this->pagination->generate_template_pagination( $pagination_url, 'pagination', 'start', $total_holders, $this->config['topics_per_page'], $start );

		// compte le nombre de types dans le groupe avocats
		$sql_count_avocat = 'SELECT count(user_id) AS avocat_count FROM ' . USER_GROUP_TABLE . ' WHERE group_id = 8 AND user_pending = 0';
		$result_count_group = $this->db->sql_query($sql_count_avocat);
		$total_in_avocat = (int)$this->db->sql_fetchfield('avocat_count');
		$this->db->sql_freeresult($result_count_group);

		// get data from configtext
		$bandeau_text_data = $this->config_text->get_array( array(
			'pharm_nb_actionnaires',
			'pharm_nb_actions',
			'pharm_percent_share',
		) );

		$this->template->assign_vars( array(
			'S_TOTAL_HOLDERS'          => $bandeau_text_data['pharm_nb_actionnaires'],
			'S_TOTAL_USERS'            => $total_users,
			'S_RATIO_HOLDERS_VS_USERS' => round( $total_holders * 100 / $total_users, 2 ) . " %",
			'U_ACTION'                 => $this->u_action,
			'S_PERCENT_SHARE'          => $bandeau_text_data['pharm_percent_share'],
			'S_TOTAL_STOCK'             => number_format($bandeau_text_data['pharm_nb_actions'], 0, ',', ' '),
			'S_TOTAL_AVOCAT'            => $total_in_avocat,
		) );



	}

	/**
	 * Set page url
	 *
	 * @param string $u_action Custom form action
	 *
	 * @return null
	 * @access public
	 */
	public function set_page_url( $u_action )
	{
		$this->u_action = $u_action;
	}

	/**
	 * R�cup�re le nombre total d'utilisateurs inscrits sur le forum
	 *
	 * @return int
	 */
	public function get_total_users()
	{
		// recupere le nombre d'utilisateurs total
		$sql2        = "SELECT COUNT(*) as total_count FROM " . USERS_TABLE . " WHERE group_id = 2 OR group_id = 3 OR group_id = 4  OR group_id = 5 OR group_id = 7";
		$result2     = $this->db->sql_query( $sql2 );
		$total_users = $this->db->sql_fetchfield( 'total_count' );
		$this->db->sql_freeresult( $result2 );

		return (int) $total_users;
	}

	/**
	 * R�cup�re le nombre total d'actionnaires d�clar�s dans la base de donn�es
	 */
	public function get_total_holders()
	{
		// recupere le nombre d'actionnaires
		$sql2          = "SELECT COUNT(*) as total_count FROM " . USERS_TABLE . " WHERE user_pharm_countowned";
		$result2       = $this->db->sql_query( $sql2 );
		$total_holders = $this->db->sql_fetchfield( 'total_count' );
		$this->db->sql_freeresult( $result2 );

		return $total_holders;
	}

}