<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 24/02/2018
 * Time: 16:12
 */

namespace jles\stockcount\controller;

class ucp_controller {

	/** @var \phpbb\log\log */
	protected $log;

	/** @var \phpbb\user */
	protected $user;

	/** @var \phpbb\db\driver\driver_interface */
	protected $db;

	/**
	 * @param   \phpbb\log\log $log
	 * @param   \phpbb\user $user
	 */
	public function __construct( \phpbb\log\log $log, \phpbb\user $user, \phpbb\db\driver\driver_interface $db)
	{
		$this->log = $log;
		$this->user = $user;
		$this->db = $db;
	}

	public function handle_log_stocks_entries($data) {

		// We want to log Admin fails to the Admin log and User fails to the user log
		$log_type = 'user';

		//check pour voir si on a ajout� ou retranch� des titres
		$current_count = $data['user_pharm_countowned'];
		$last_count = $data['pharm_lastowned_stocks'];
		$diff = $current_count - $last_count;

		$additional_data = array();
		$end_msg = " %s ".$this->user->lang('UCP_PHARM_LOG_STOCKS')." - ".$this->user->lang('UCP_PHARM_LOG_STOCKSOWNED')." : %s</strong>";
		$skip = false;

		// on change le message en fonction de si on ajoute ou retranche des actions dans le profil user
		if ($diff > 0) { // on a ajout� des titres
			$additional_data[] = sprintf("<strong> ".$this->user->lang('UCP_PHARM_LOG_STOCKS_ADD').$end_msg, $diff, $current_count);
		} else if ($diff < 0) { // on a retranch� des titres
			$additional_data[] = sprintf("<strong> ".$this->user->lang('UCP_PHARM_LOG_STOCKS_REMOVE').$end_msg, $diff, $current_count);
		} else { // rien n'a boug�
			$skip = true; // saute le log
		}


		if (!$skip) {
			$this->log->add($log_type, $this->user->data['user_id'], $this->user->ip, $this->user->data['username'], time(), $additional_data);
			// met a jour le champ last edited dans la base de donn�es
			$this->db->sql_query('UPDATE ' . USERS_TABLE . ' SET user_pharm_last_edited = ' . time() . ' WHERE user_id = ' . (int)$this->user->data['user_id']);
		}
	}
}
