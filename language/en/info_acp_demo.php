<?php
/**
 *
 * Compte Actions. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2018, Jack Lestrohan
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ACP_STOCKCOUNT_EXT_NAME'			=> 'Stock Number Extension',
	'ACP_EXT_TITLE'                     => 'Stock Number Extension', // for retro-compatibility
	'ACP_PARAMS'					    => 'Settings',

	// logs
	'ACP_STOCKCOUNT_SETTINGS_LOG'	=> '<strong>Pharming settings modification</strong>',

	'ACP_ACTIVATE_EXTENSION'	            => 'Activate the Module',
	'ACP_PHARMING_SETTING_SAVED'	        => 'Settings have been saved successfully!',
	'ACP_EXPLAIN_STOCK_TOTAL'               => "Used in computing the total stock share owned by the users vs global stocks available",
	'ACP_EXPLAIN_REMIND_MSG'                => "Warn users with a recuring banner when their stock total is empty while they have ticked the own pharming stocks to yes",

	'ACP_GRAPHICAL_OPTIONS'                 => "Graphical options",
	'ACP_DISPLAY_BANNER'                    => "Display the info banner above the forums",
	'ACP_EXPLAIN_DISPLAY_BANNER'            => "Activates the displaying of the banner on the website",
	'ACP_BANNER_REFRESH_DELAY'              => "Auto updating banner refresh delay (in seconds)",
	'ACP_EXPLAIN_BANNER_REFRESH_DELAY'      => "How many seconds between each auto-refresh<br/>(Warning may impact server's performances).",

	'ACP_SOIT'                             => "or",

	'ACP_TOTAL_PHARMING_MARKET_STOCKS'      => 'Total number of Pharming Stocks on the market',

	'ACP_CREDITS_JACK1'                     => "Pharming Stocks Count Extension",
	'ACP_CREDITS_ALL_RIGHTS_RESERVED'       => "All Rights Reserved",
	'ACP_CREDITS_SPECIALLY_CREATED'         => 'Especially created for the Pharming Group Stock Holders Community',

	// stockcount list top10 users
	'ACP_HOLDERS_TITLE'                    => 'Pharming Stock holders List',
	'ACP_HOLDERS'                        => 'Pharming Share holders List',
	'ACP_HOLDERS_USERNAME_TITLE'             => 'Username',
	'ACP_HOLDERS_STOCK_COUNT'               => 'Nb Actions',
	'ACP_HOLDERS_F1'                        => 'Friend 1',
	'ACP_HOLDERS_F2'                        => 'Friend 2',
	'ACP_HOLDERS_F3'                        => 'Friend 3',
	'ACP_HOLDERS_F4'                        => 'Friend 4',
	'ACP_HOLDERS_F5'                        => 'Friend 5',
	'ACP_HOLDERS_TOTAL_DIFF'                => 'Difference Last Edition',
	'ACP_HOLDERS_DATE_LAST_EDITION'         => 'Last edited on',

	// errors
	'ACP_STOCKCOUNT_EMPTY_TOTAL_STOCK'      => 'The Pharming total stock number field cannot be empty'
));
