<?php
/**
 *
 * Compte Actions. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2018, Jack Lestrohan
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'UCP_PHARMING_STOCK'				=> 'Pharming Stocks',
	'UCP_PHARMING_TITLE'		        => 'Pharming Stocks',
	'UCP_PHARM_OWNED'			        => "I currently own Pharming stocks",
	'UCP_PHARM_OWNED_EXPLAIN'           => "Set to yes if you or any of your friends/relatives below are Pharming stock holders",
	'UCP_PHARMING_SAVED'	            => 'Pharming Stocks Settings have been saved successfully!',

	'UCP_PHARM_NUMBER_OWNED'            =>  "Quantity of owned Pharming Group stocks",
	'UCP_PHARM_NUMBER_OWNED_EXPLAIN'    =>  "Total number of Pharming Group stocks you currently own",
	'UCP_PHARM_NUMBER_FRIEND1'          =>  "Quantity of close friends's/relative's #1",
	'UCP_PHARM_NUMBER_FRIEND2'          =>  "Quantity of close friends's/relative's #2",
	'UCP_PHARM_NUMBER_FRIEND3'          =>  "Quantity of close friends's/relative's #3",
	'UCP_PHARM_NUMBER_FRIEND4'          =>  "Quantity of close friends's/relative's #4",
	'UCP_PHARM_NUMBER_FRIEND5'          =>  "Quantity of close friends's/relative's #5",
	'UCP_PHARM_NUMBER_FRIEND_EXPLAIN'   =>  "If someone of your entourage owns Pharming stocks you can use this field to add their stocks",

	'UCP_EXPLAIN_MODULE'                => 'You may want to fill up/update the fields below in order for the website to sum up the total user-owned Pharming stock options',

	'UCP_PHARM_LOG_STOCKS_ADD'          => 'Added',
	'UCP_PHARM_LOG_STOCKS_REMOVE'       => 'Removed',
	'UCP_PHARM_LOG_STOCKSOWNED'              => 'Current total amount',
	'UCP_PHARM_LOG_STOCKS'              => 'pharming stocks',
	'UCP_PHARM_INFO_STOCK_ACTIVITY'     => ' Operation on shares'
));
