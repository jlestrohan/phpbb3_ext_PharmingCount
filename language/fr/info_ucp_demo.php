<?php
/**
 *
 * Compte Actions. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2018, Jack Lestrohan
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'UCP_PHARMING_STOCK'	            => 'Actions Pharming',
	'UCP_PHARMING_TITLE'		        => 'Actions Pharming',
	'UCP_PHARM_OWNED'			        => "Je poss&egrave;de actuellement des actions Pharming",
	'UCP_PHARM_OWNED_EXPLAIN'           => "D&eacute;clarez si vous ou vos amis ci-dessous &ecirc;tes actionnaires (essentiel pour prise en compte du calcul total, tout user d&eacute;clarant non ne sera pas comptabilis&eacute;)",
	'UCP_PHARMING_SAVED'		        => 'Vos informations sur les actions Pharming ont &eacute;t&eacute; sauvegard&eacute;es. Merci!',

	'UCP_PHARM_NUMBER_OWNED'            =>  "Quantit&eacute; de mes actions pharming group",
	'UCP_PHARM_NUMBER_OWNED_EXPLAIN'    =>  "Nombre d'actions Pharming dont vous disposez actuellement",
	'UCP_PHARM_NUMBER_FRIEND1'          =>  "Quantit&eacute; d'action d'un proche #1",
	'UCP_PHARM_NUMBER_FRIEND2'          =>  "Quantit&eacute; d'action d'un proche #2",
	'UCP_PHARM_NUMBER_FRIEND3'          =>  "Quantit&eacute; d'action d'un proche #3",
	'UCP_PHARM_NUMBER_FRIEND4'          =>  "Quantit&eacute; d'action d'un proche #4",
	'UCP_PHARM_NUMBER_FRIEND5'          =>  "Quantit&eacute; d'action d'un proche #5",
	'UCP_PHARM_NUMBER_FRIEND_EXPLAIN'   =>  "Nombre d'actions Pharming dont un de vos proche ou ami dispose actuellement",

	'UCP_EXPLAIN_MODULE'                => "Vous pouvez, si vous le souhaitez, renseigner et/ou mettre &agrave; jour les champs ci-dessous pour indiquer au site le nombre d'actions que vous, ou vos amis, d&eacute;tenez afin de pouvoir faire un d&eacute;compte global pr&eacute;cis du capital entre les mains des actionnaires de ce forum.",

	'UCP_PHARM_LOG_STOCKS_ADD'          => 'Ajout de',
	'UCP_PHARM_LOG_STOCKS_REMOVE'       => 'Retrait de',
	'UCP_PHARM_LOG_STOCKSOWNED'              => 'Total actuel',
	'UCP_PHARM_LOG_STOCKS'              => 'actions Pharming',
	'UCP_PHARM_INFO_STOCK_ACTIVITY'     => ' Mouvement sur les actions'

));
