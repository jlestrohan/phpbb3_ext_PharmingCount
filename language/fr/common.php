<?php
/**
 *
 * Compte Actions. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2018, Jack Lestrohan
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(

	'PHARMING_STOCK_HOLDER'                     => 'Actionnaire Pharming',
	'STOCK_HOLDERS'                             => 'actionnaires',
	'SHARES'                                    => 'actions',
	'OF_TOTAL_CAPITAL'                          => 'du capital'


));
