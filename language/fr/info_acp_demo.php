<?php
/**
 *
 * Compte Actions. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2018, Jack Lestrohan
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ACP_STOCKCOUNT_EXT_NAME'			=> 'Extension Pharming',
	'ACP_EXT_TITLE'                     => 'Extension Pharming', // pour retro compatibilité
	'ACP_PARAMS'			=> 'Param&egrave;tres Pharming',


	// logs
	'ACP_STOCKCOUNT_SETTINGS_LOG'	=> '<strong>Paramètres de Pharming modifiés</strong>',

	'ACP_ACTIVATE_EXTENSION'	            => 'Activer le Module',
	'ACP_PHARMING_SETTING_SAVED'	        => 'Settings have been saved successfully!',
	'ACP_EXPLAIN_STOCK_TOTAL'               => "Utilis&eacute; pour calculer le pourcentage d'actions Pharming d&eacutetenues par les membres et leurs amis",
	'ACP_EXPLAIN_REMIND_MSG'                => "Afficher un message de fa&ccedil;on r&eacute;guli&egrave;re aux utilisateurs qui ont un nombre d'actions<br/>pharming &agrave; z&eacute;ro alors qu'ils ont coch&eacute; la case possesseur d'actions pour eux ou leurs famille/amis",

	'ACP_GRAPHICAL_OPTIONS'                 => "Options graphiques",
	'ACP_DISPLAY_BANNER'                    => "Affichage du bandeau au dessus du forum",
	'ACP_EXPLAIN_DISPLAY_BANNER'            => "Active l'affichage de la banni&egrave;re au dessus du forum. Non = rien ne sera affich&eacute;.",
	'ACP_BANNER_REFRESH_DELAY'              => "D&eacute;lai de rafraichissement pour la mise &agrave; jour des chiffres de la banni&egrave;re (en secondes)",
	'ACP_EXPLAIN_BANNER_REFRESH_DELAY'      => "Nombre de secondes entre chaque rafraichissement de la banni&egrave;re<br/>(Attention: peut impacter les performances du serveur).",

	'ACP_SOIT'                             => "soit",

	'ACP_TOTAL_PHARMING_MARKET_STOCKS'      => "Nombre total d'actions Pharming sur le march&eacute;",

	'ACP_CREDITS_JACK1'                     => "Extension Compte d'actions Pharming",
	'ACP_CREDITS_ALL_RIGHTS_RESERVED'       => "Tous Droits R&eacute;serv&eacute;s",
	'ACP_CREDITS_SPECIALLY_CREATED'         => "Cr&eacute;&eacute;e sp&eacute;cialement pour le forum actionnaires-pharming-group",

	// stockcount list holders users
	'ACP_HOLDERS_TITLE'                      => 'Tableau des possesseurs d\'actions Pharming',
	'ACP_HOLDERS'                           => 'Liste des d&eacute;tenteurs',
	'ACP_HOLDERS_USERNAME_TITLE'             => 'Pseudo',
	'ACP_HOLDERS_STOCK_COUNT'               => 'Nb Actions',
	'ACP_HOLDERS_F1'                        => 'Ami 1',
	'ACP_HOLDERS_F2'                        => 'Ami 2',
	'ACP_HOLDERS_F3'                        => 'Ami 3',
	'ACP_HOLDERS_F4'                        => 'Ami 4',
	'ACP_HOLDERS_F5'                        => 'Ami 5',
	'ACP_HOLDERS_TOTAL_DIFF'                => 'Difference',
	'ACP_HOLDERS_DATE_LAST_EDITION'         => 'Date &eacute;dition',


	// errors
	'ACP_STOCKCOUNT_EMPTY_TOTAL_STOCK'      => 'Le champ Nombre total d\'actions Pharming ne peut rester vide'
));