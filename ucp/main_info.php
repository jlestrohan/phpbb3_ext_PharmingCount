<?php
/**
 *
 * Compte Actions. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2018, Jack Lestrohan
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace jles\stockcount\ucp;

/**
 * Compte Actions UCP module info.
 */
class main_info
{
	function module()
	{
		return array(
			'filename'	=> '\jles\stockcount\ucp\main_module',
			'title'		=> 'UCP_PHARMING_TITLE',
			'modes'		=> array(
				'settings'	=> array(
					'title'	=> 'UCP_PHARMING_STOCK',
					'auth'	=> 'ext_jles/stockcount',
					'cat'	=> array('UCP_PHARMING_TITLE')
				),
			),
		);
	}
}
