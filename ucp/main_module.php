<?php
/**
 *
 * Compte Actions. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2018, Jack Lestrohan
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace jles\stockcount\ucp;

/**
 * Compte Actions UCP module.
 */
class main_module
{
	var $u_action;

	/** @var \phpbb\config\db_text */
	protected $config_text;

	protected $language;

	protected $phpbb_container;


	public function __construct()
	{
		global $phpbb_container;
		$this->config_text = $phpbb_container->get('config_text');
		$this->language = $phpbb_container->get('language');
		$this->phpbb_container = $phpbb_container;
	}

	function main($id, $mode)
	{
		global $db, $request, $template, $user;

		$this->tpl_name = 'ucp_pharming_body';
		$this->page_title = $this->language->lang('UCP_PHARMING_TITLE');
		add_form_key('jles/pharming');

		$data = array(
			// r�cup�re les infos de variable tra�ant les valeurs pr�c�dentes
			'pharm_lastowned_stocks' => $user->data['user_pharm_countowned'],
			'pharm_lastfriend1_stocks' => $user->data['user_pharm_countfriend1'],
			'pharm_lastfriend2_stocks' => $user->data['user_pharm_countfriend2'],
			'pharm_lastfriend3_stocks' => $user->data['user_pharm_countfriend3'],
			'pharm_lastfriend4_stocks' => $user->data['user_pharm_countfriend4'],
			'pharm_lastfriend5_stocks' => $user->data['user_pharm_countfriend5'],

			'user_ownpharm' => $request->variable('user_ownpharm', $user->data['user_ownpharm']),
			'user_pharm_countowned' => abs($request->variable('user_pharm_countowned', $user->data['user_pharm_countowned'])),
			'user_pharm_countfriend1' => abs($request->variable('user_pharm_countfriend1', $user->data['user_pharm_countfriend1'])),
			'user_pharm_countfriend2' => abs($request->variable('user_pharm_countfriend2', $user->data['user_pharm_countfriend2'])),
			'user_pharm_countfriend3' => abs($request->variable('user_pharm_countfriend3', $user->data['user_pharm_countfriend3'])),
			'user_pharm_countfriend4' => abs($request->variable('user_pharm_countfriend4', $user->data['user_pharm_countfriend4'])),
			'user_pharm_countfriend5' => abs($request->variable('user_pharm_countfriend5', $user->data['user_pharm_countfriend5'])),

			'user_pharm_namefriend1' => $request->variable('user_pharm_namefriend1', $user->data['user_pharm_namefriend1']),
			'user_pharm_namefriend2' => $request->variable('user_pharm_namefriend2', $user->data['user_pharm_namefriend2']),
			'user_pharm_namefriend3' => $request->variable('user_pharm_namefriend3', $user->data['user_pharm_namefriend3']),
			'user_pharm_namefriend4' => $request->variable('user_pharm_namefriend4', $user->data['user_pharm_namefriend4']),
			'user_pharm_namefriend5' => $request->variable('user_pharm_namefriend5', $user->data['user_pharm_namefriend5']),


		);

		if ($request->is_set_post('submit'))
		{
			if (!check_form_key('jles/pharming'))
			{
				trigger_error($user->lang('FORM_INVALID'));
			}

			$sql = 'UPDATE ' . USERS_TABLE . '
				SET ' . $db->sql_build_array('UPDATE', $data) . '
				WHERE user_id = ' . $user->data['user_id'];
			$db->sql_query($sql);

			$this->update_stockinfos();

			meta_refresh(3, $this->u_action);
			$message = $this->language->lang('UCP_PHARMING_SAVED') . '<br /><br />' . $this->language->lang('RETURN_UCP', '<a href="' . $this->u_action . '">', '</a>');

			// r�cup�re le service depuis le container
			$controller = $this->phpbb_container->get('jles.stockcount.ucp.controller');

			// appel au controller pour g�rer les logs, on passe l'ensemble des datas
			$controller->handle_log_stocks_entries($data);

			trigger_error($message);


		}

		$template->assign_vars(array(
			'S_USER_OWNPHARM'	        => $data['user_ownpharm'],
			'S_USER_PHARM_COUNTOWNED'   => $data['user_pharm_countowned'],
			'S_USER_PHARM_COUNTFRIEND1' => $data['user_pharm_countfriend1'],
			'S_USER_PHARM_COUNTFRIEND2' => $data['user_pharm_countfriend2'],
			'S_USER_PHARM_COUNTFRIEND3' => $data['user_pharm_countfriend3'],
			'S_USER_PHARM_COUNTFRIEND4' => $data['user_pharm_countfriend4'],
			'S_USER_PHARM_COUNTFRIEND5' => $data['user_pharm_countfriend5'],

			'S_USER_PHARM_NAMEFRIEND1' => $data['user_pharm_namefriend1'],
			'S_USER_PHARM_NAMEFRIEND2' => $data['user_pharm_namefriend2'],
			'S_USER_PHARM_NAMEFRIEND3' => $data['user_pharm_namefriend3'],
			'S_USER_PHARM_NAMEFRIEND4' => $data['user_pharm_namefriend4'],
			'S_USER_PHARM_NAMEFRIEND5' => $data['user_pharm_namefriend5'],

			'S_UCP_ACTION'	    => $this->u_action,
		));

	}

	private function update_stockinfos ()
	{
		global $db, $config;

		// Compute how many stockers have declared owniong pharming stocks
		$sql = 'SELECT COUNT(user_id) AS actionnaires_count
        FROM ' . USERS_TABLE . '
        WHERE user_ownpharm = TRUE';
		$result = $db->sql_query($sql);
		// The number of actionnaires is now available here:
		$actionnaires_count = (int) $db->sql_fetchfield('actionnaires_count');
		$db->sql_freeresult($result);


		// Compute how many total stocks
		$sql1 = 'SELECT
						SUM(user_pharm_countowned) +
						SUM(user_pharm_countfriend1) +
						SUM(user_pharm_countfriend2) +
						SUM(user_pharm_countfriend3) +
						SUM(user_pharm_countfriend4) +
						SUM(user_pharm_countfriend5) AS	Total
        FROM ' . USERS_TABLE . '
        WHERE user_ownpharm = TRUE';
		$result = $db->sql_query($sql1);
		// the sum of the total stocks is available here:
		$owned_stocks = $db->sql_fetchfield('Total');
		$db->sql_freeresult($result);

		// astuce si jamais le nombre d'actions total pharming n'a pas �t� renseign�
		if ((int)$config['acp_pharm_total_stock_available'] > 0) {
			$percent_share = number_format(100 * $owned_stocks / (int) $config['acp_pharm_total_stock_available'], 5);
		} else $percent_share = 0;

	    // Store the Pharming calculated/updated fields to the config_table in the database
		$this->config_text->set_array(array(
			'pharm_nb_actionnaires'         => $actionnaires_count,
			'pharm_nb_actions'              => $owned_stocks,
			'pharm_percent_share'           => $percent_share,
		));
	}


}
