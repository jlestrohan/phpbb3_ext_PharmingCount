/**
 * Created by jack on 27/02/2018.
 */



<!-- IF not $INCLUDED_JQUERYUIJS -->
<!-- INCLUDEJS @jles_stockcount/jquery-3.3.1.min.js -->
<!-- DEFINE $INCLUDED_JQUERYUIJS = true -->
<!-- ENDIF -->


$(document).ready(function () {


    // ajoute un div pour le contenu a wrap dans le collapsible mchat
    $(".mchat-wrapper").wrap("<div id='mchatwrap'></div>");


    //$('.mchat-wrapper').attr('id', 'mchatwrap');
    $('.index-right').attr('id', 'recentright');

    // ajoute les signes +, -
    var topiclist = $('.mchat-wrapper').find('.topiclist');
    topiclist.after('<div class="collapse-trigger open"><span class="icon fa-minus tooltip-left" title="Collapse"></span><span class="icon fa-plus tooltip-left" title="Expand"></span></div>');


    var recentsubjects = $('#recent-topics-box');
    recentsubjects.find('ul:first').after('<div class="collapse-trigger open"><span class="icon fa-minus tooltip-left tooltipstered" title="Collapse"></span><span class="icon fa-plus tooltip-left tooltipstered" title="Expand"></span></div>');

//


    // Collapsible Panels

        $("#mchatwrap").collapse({
            query: '.collapse-trigger',
            persist: true,
            open: function () {
                // The context of 'this' is applied to
                // the collapsed details in a jQuery wrapper
                this.slideDown(100);
            },
            close: function () {
                this.slideUp(100);
            }
        });

        $('#recenttopicstop').collapse({
            query: '.collapse-trigger',
            persist: true,
            open: function () {
                // The context of 'this' is applied to
                // the collapsed details in a jQuery wrapper
                this.slideDown(100);
            },
            close: function () {
                this.slideUp(100);
            }
        });

        $('#recenttopicbottom').collapse({
            query: '.collapse-trigger',
            persist: true,
            open: function () {
                // The context of 'this' is applied to
                // the collapsed details in a jQuery wrapper
                this.slideDown(100);
            },
            close: function () {
                this.slideUp(100);
            }
        });

        $("#recentright").collapse({
            query: '.collapse-trigger',
            persist: true,
            open: function () {
                // The context of 'this' is applied to
                // the collapsed details in a jQuery wrapper
                this.slideDown(100);
            },
            close: function () {
                this.slideUp(100);
            },
        });


});

