/**
 * Created by jack on 24/02/2018.
 */

<!-- IF not $INCLUDED_JQUERYUIJS -->
    <!-- INCLUDEJS @jles_stockcount/jquery-3.3.1.min.js -->
    <!-- DEFINE $INCLUDED_JQUERYUIJS = true -->
<!-- ENDIF -->

(function($) {  // Avoid conflicts with other libraries
    'use strict';
    $(function() {
            window.setInterval(function() {
                $('#bandeau_actionnaires').load(stockcount_ajax.bandeau_actionnaires_url);
            }, stockcount_ajax.bandeau_actionnaires_refresh_delay);
    })
})(jQuery); // Avoid conflicts with other libraries