<?php
/**
 *
 * Compte Actions. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2018, Jack Lestrohan
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace jles\stockcount\migrations;

class stocklogs_migration extends \phpbb\db\migration\migration
{

	static public function depends_on()
	{
		return array('\jles\stockcount\migrations\install_user_schema');
	}

	public function update_schema()
	{
		return array(
			'add_columns'	=> array(
				$this->table_prefix . 'users'			=> array(
					'pharm_lastowned_stocks'	    			=> array('UINT:15', 0),
					'pharm_lastfriend1_stocks'				=> array('UINT:15', 0),
					'pharm_lastfriend2_stocks'				=> array('UINT:15', 0),
					'pharm_lastfriend3_stocks'				=> array('UINT:15', 0),
					'pharm_lastfriend4_stocks'				=> array('UINT:15', 0),
					'pharm_lastfriend5_stocks'				=> array('UINT:15', 0),
				),
			),
		);
	}

	public function revert_schema()
	{
		return array(
			'drop_columns'	=> array(
				$this->table_prefix . 'users'			=> array(
					'pharm_lastowned_stocks',
					'pharm_lastfriend1_stocks',
					'pharm_lastfriend2_stocks',
					'pharm_lastfriend3_stocks',
					'pharm_lastfriend4_stocks',
					'pharm_lastfriend5_stocks',
				),
			),
		);
	}
}
