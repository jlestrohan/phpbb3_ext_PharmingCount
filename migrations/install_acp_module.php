<?php
/**
 *
 * Compte Actions. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2018, Jack Lestrohan
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace jles\stockcount\migrations;

class install_acp_module extends \phpbb\db\migration\migration
{
	public function effectively_installed()
	{
		return isset($this->config['acp_pharm_activate_extension']);
	}

	static public function depends_on()
	{
		return array('\phpbb\db\migration\data\v31x\v314');
	}

	public function update_data()
	{
		return array(
			array('config.add', array('acp_pharm_activate_extension', 0)),
			array('config.add', array('acp_pharm_total_stock_available', 0)),
			array('config.add', array('acp_pharm_remind_useremail', 0)),

			array('config_text.add', array('pharm_nb_actionnaires', 0)),
			array('config_text.add', array('pharm_nb_actions', 0)),
			array('config_text.add', array('pharm_percent_share', 0)),

			array('module.add', array(
				'acp',
				'ACP_CAT_DOT_MODS',
				'ACP_EXT_TITLE'
			)),
			array('module.add', array(
				'acp',
				'ACP_EXT_TITLE',
				array(
					'module_basename'	=> '\jles\stockcount\acp\main_module',
					'modes'				=> array('settings'),
				),
			)),
		);
	}
}
