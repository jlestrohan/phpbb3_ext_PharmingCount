<?php
/**
 *
 * Compte Actions. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2018, Jack Lestrohan
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace jles\stockcount\migrations;

class install_user_schema extends \phpbb\db\migration\migration
{
	public function effectively_installed()
	{
		return  $this->db_tools->sql_column_exists($this->table_prefix . 'users', 'user_ownpharm') and
		        $this->db_tools->sql_column_exists($this->table_prefix . 'users', 'user_pharm_countowned') and
				$this->db_tools->sql_column_exists($this->table_prefix . 'users', 'user_pharm_countfriend1') and
				$this->db_tools->sql_column_exists($this->table_prefix . 'users', 'user_pharm_countfriend2') and
				$this->db_tools->sql_column_exists($this->table_prefix . 'users', 'user_pharm_countfriend3') and
				$this->db_tools->sql_column_exists($this->table_prefix . 'users', 'user_pharm_countfriend4') and
				$this->db_tools->sql_column_exists($this->table_prefix . 'users', 'user_pharm_countfriend5');
	}

	static public function depends_on()
	{
		return array('\phpbb\db\migration\data\v31x\v314');
	}

	public function update_schema()
	{
		return array(

			'add_columns'	=> array(
				$this->table_prefix . 'users'			=> array(
					'user_ownpharm'                         => array('BOOL', 0),
					'user_pharm_countowned'	    			=> array('UINT:15', 0),
					'user_pharm_countfriend1'				=> array('UINT:15', 0),
					'user_pharm_countfriend2'				=> array('UINT:15', 0),
					'user_pharm_countfriend3'				=> array('UINT:15', 0),
					'user_pharm_countfriend4'				=> array('UINT:15', 0),
					'user_pharm_countfriend5'				=> array('UINT:15', 0),

					'user_pharm_namefriend1'				=> array('VCHAR:32', ''),
					'user_pharm_namefriend2'				=> array('VCHAR:32', ''),
					'user_pharm_namefriend3'				=> array('VCHAR:32', ''),
					'user_pharm_namefriend4'				=> array('VCHAR:32', ''),
					'user_pharm_namefriend5'				=> array('VCHAR:32', ''),
				),
			),
		);
	}

	public function revert_schema()
	{
		return array(
			'drop_columns'	=> array(
				$this->table_prefix . 'users'			=> array(
					'user_ownpharm',
					'user_pharm_countowned',
					'user_pharm_countfriend1',
					'user_pharm_countfriend2',
					'user_pharm_countfriend3',
					'user_pharm_countfriend4',
					'user_pharm_countfriend5',

					'user_pharm_namefriend1',
					'user_pharm_namefriend2',
					'user_pharm_namefriend3',
					'user_pharm_namefriend4',
					'user_pharm_namefriend5'
				),
			),
			'drop_tables'		=> array(
				$this->table_prefix . 'pharmingstock',
			),
		);
	}
}
