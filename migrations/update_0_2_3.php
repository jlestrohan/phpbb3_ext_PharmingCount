<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 24/02/2018
 * Time: 10:25
 */

namespace jles\stockcount\migrations;
use phpbb\db\migration\migration;


class update_0_2_3 extends migration
{

	static public function depends_on()
	{
		return array('\jles\stockcount\migrations\install_user_schema');
	}

	public function update_data()
	{
		return array(
			array( 'config.add', array( 'acp_pharm_ajax_allow_update', 1 ) ),
			array( 'config.add', array( 'acp_pharm_banner_refresh_delay', 5000 ) ), //5 seconds default

			array( 'config.remove', array( 'acp_pharm_remind_useremail' ) ),
			array( 'config.remove', array( 'acp_pharm_warning_threshold' ) ),
			array( 'config.remove', array( 'acp_pharm_warn_over_threshold' ) ),
			array( 'config.remove', array( 'acp_pharm_warn_over_three' ) ),
			array(
				'module.add',
				array(
					'acp',
					'ACP_EXT_TITLE',
					array(
						'module_basename' => '\jles\stockcount\acp\main_module',
						'modes'           => array( 'holders' ),
					),
				)
			),
		);
	}

	public function update_schema()
	{
		return array(
			'add_columns'	=> array(
				$this->table_prefix . 'users'			=> array(
					'user_pharm_last_edited'            => array('UINT:11', 0),
				),
			),
		);
	}

	public function revert_schema()
	{
		return array(
			'drop_columns'	=> array(
				$this->table_prefix . 'users'			=> array(
					'user_pharm_last_edited'
				),
			),
		);
	}

}