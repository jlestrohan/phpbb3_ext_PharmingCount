# Compte Actions

## Installation

Copy the extension to phpBB/ext/jles/stockcount

Go to "ACP" > "Customise" > "Extensions" and enable the "Compte Actions" extension.

## License

[GPLv2](license.txt)
