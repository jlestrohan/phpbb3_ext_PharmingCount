<?php
/**
 *
 * Compte Actions. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2018, Jack Lestrohan
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace jles\stockcount\acp;

/**
 * Compte Actions ACP module info.
 */
class main_info
{
	public function module()
	{
		return array(
			'filename'	=> '\jles\stockcount\acp\main_module',
			'title'		=> 'ACP_STOCKCOUNT_EXT_NAME',
			'modes'		=> array(
				'settings'	=> array(
					'title'	=> 'ACP_PARAMS',
					'auth'	=> 'ext_jles/stockcount && acl_a_board',
					'cat'	=> array('ACP_STOCKCOUNT_EXT_NAME')
				),
				'holders'    => array(
					'title' => 'ACP_HOLDERS',
					'auth'  => 'ext_jles/stockcount && acl_a_board',
					'cat'   => array('ACP_STOCKCOUNT_EXT_NAME')
				)
			),
		);
	}
}
