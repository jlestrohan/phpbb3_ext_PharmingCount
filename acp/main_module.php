<?php
/**
 *
 * Compte Actions. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2018, Jack Lestrohan
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace jles\stockcount\acp;

/**
 * Compte Actions ACP module.
 */
class main_module
{
	public $page_title;
	public $tpl_name;
	public $u_action;

	public function main($id, $mode)
	{
		global $phpbb_container;

		/** @var \phpbb\language\language $lang */
		$lang = $phpbb_container->get('language');

		/** @var \phpbb\request\request $request */
		$request = $phpbb_container->get('request');

		// Get an instance of the admin controller
		$admin_controller = $phpbb_container->get('jles.stockcount.admin.controller');

		// Make the $u_action url available in the admin controller
		$admin_controller->set_page_url($this->u_action);

		switch ($mode)
		{
			case 'settings':
				// Load a template from adm/style for our ACP page
				$this->tpl_name = 'acp_stockcount_settings';

				// Set the page title for our ACP page
				$this->page_title = $lang->lang('ACP_STOCKCOUNT_EXT_NAME');

				// Load the display options handle in the admin controller
				$admin_controller->display_options();

				break;

			case 'holders':
				// Load a template from adm/style for our ACP page
				$this->tpl_name = 'acp_stockcount_holders';

				// Set the page title for our ACP page
				$this->page_title = $lang->lang('ACP_STOCKCOUNT_EXT_NAME');

				// Load the display options handle in the admin controller
				$admin_controller->display_holders();

				break;
		}
	}
}
