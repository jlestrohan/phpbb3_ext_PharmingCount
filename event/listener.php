<?php
/**
 *
 * Compte Actions. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2018, Jack Lestrohan
 * @license       GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace jles\stockcount\event;

/**
 * @ignore
 */
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class listener implements EventSubscriberInterface
{
	/** @var \phpbb\config\db_text */
	protected $config_text;

	/** @var \phpbb\template\template */
	protected $template;

	/** @var \phpbb\config\config */
	protected $config;

	/** @var \phpbb\user */
	protected $user;

	/** @var \phpbb\controller\helper */
	protected $helper;

	/** @var \phpbb\db\driver\driver_interface */
	protected $db;


	/**
	 * Constructor
	 *
	 * @param \phpbb\template\template          $template
	 * @param \phpbb\config\config              $config
	 * @param \phpbb\config\db_text             $config_text
	 * @param \phpbb\user                       $user
	 * @param \phpbb\controller\helper          $helper
	 * @param \phpbb\log\log                    $log
	 * @param \phpbb\db\driver\driver_interface $db
	 */
	public function __construct(
		\phpbb\template\template $template, \phpbb\config\config $config, \phpbb\config\db_text $config_text,
		\phpbb\user $user, \phpbb\controller\helper $helper, \phpbb\log\log $log, \phpbb\db\driver\driver_interface $db
	) {
		$this->template    = $template;
		$this->config      = $config;
		$this->config_text = $config_text;
		$this->user        = $user;
		$this->helper      = $helper;
		$this->db          = $db;
	}

	/**
	 * Assign functions defined in this class to event listeners in the core
	 *
	 * @return array
	 */
	static public function getSubscribedEvents()
	{
		return array(
			'core.page_header_after'      => 'get_infos_from_configtext',
			'core.user_setup'             => 'load_language_on_setup',
			'core.user_active_flip_after' => 'set_user_not_own_pharm',
			'core.page_footer'            => 'core_page_footer',
		);
	}

	/**
	 * @param data $event
	 */
	public function load_language_on_setup( $event )
	{
		$lang_set_ext          = $event['lang_set_ext'];
		$lang_set_ext[]        = array(
			'ext_name' => 'jles/stockcount',
			'lang_set' => 'common',
		);
		$event['lang_set_ext'] = $lang_set_ext;
	}

	public function get_infos_from_configtext()
	{

		// get data from configtext
		$bandeau_text_data = $this->config_text->get_array( array(
			'pharm_nb_actionnaires',
			'pharm_nb_actions',
			'pharm_percent_share',
			'pharm_display_banner'
		) );

		$this->template->assign_vars( array(
			'S_NB_ACTIONNAIRES'             => $bandeau_text_data['pharm_nb_actionnaires'],
			'S_NB_ACTIONS'                  => number_format( $bandeau_text_data['pharm_nb_actions'], 0, ',', ' ' ),
			'S_PERCENT_SHARE'               => $bandeau_text_data['pharm_percent_share'],
			'S_ACP_PHARM_DISPLAY_BANNER'    => $this->config['acp_pharm_display_banner'],
			'S_USER_OWNS'                   => $this->user->data['user_ownpharm'],
			'S_STOCKCOUNT_AJAX_ALLOWUPDATE' => $this->config['pharm_ajax_allow_update'],
			'U_STOCKCOUNT_BANDEAU_URL'      => $this->helper->route( 'jles_stockcount_bandeau_actionnaires' ),
			'S_STOCKCOUNT_REFRESH_DELAY'    => $this->config['acp_pharm_banner_refresh_delay'] > 0 ? $this->config['acp_pharm_banner_refresh_delay'] : 10000
			// defaut 10000
		) );
	}

	/**
	 * met le flag de user_own_pharm a zero � l'utilisateur concern�
	 *
	 * @param $event
	 */
	public function set_user_not_own_pharm( $event )
	{
		// place tous les flags user_own_pharm des membres suivants � z�ro
		foreach ( $event['user_id_ary'] as $userID )
		{
			$sql2 = "UPDATE " . USERS_TABLE . " SET user_ownpharm = 0 WHERE user_id = " . (int) $userID;
			$result = $this->db->sql_query( $sql2 );
			$this->db->sql_freeresult($result);
		}
	}

	/**
	 * Ajoute une template variable pour savoir si un user est dans un certain groupe
	 * <!-- IF S_GROUP_5 --> This is only visible for members of the group with id 5 <!-- ENDIF -->
	 *
	 * @param $event
	 */
	public function core_page_footer($event)
	{
		$template_vars = array();
		$user_id = $this->user->data['user_id'];

		$sql = 'SELECT group_id
			FROM ' . USER_GROUP_TABLE . '
			WHERE user_id = ' . $user_id . '
				AND user_pending = 0';

		$result = $this->db->sql_query($sql);

		while ($group_id = $this->db->sql_fetchfield('group_id'))
		{
			$template_vars['S_GROUP_' . $group_id] = true;
		}

		$this->db->sql_freeresult($result);

		if (sizeof($template_vars))
		{
			$this->template->assign_vars($template_vars);
		}
	}
}